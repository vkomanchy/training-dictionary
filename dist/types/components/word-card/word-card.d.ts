import { EventEmitter } from '../../stencil-public-runtime';
import { BaseInputCustomEvent } from '../../components';
export declare class WordCard {
  wordId: string;
  original: string;
  transcription?: string;
  translation: string;
  translateGuess: string;
  guessType: 'repeatWords' | 'reverseRepeatWords';
  isOpen: boolean;
  deleteCard: EventEmitter<{
    wordId: string;
  }>;
  editCard: EventEmitter<{}>;
  handleCloseEdit: () => void;
  handleClickEdit: () => void;
  handleClickDelete: () => void;
  handleChangeTranslationGuess: (event: BaseInputCustomEvent<{
    value: string;
  }>) => void;
  render(): any;
}

import { EventEmitter } from '../../stencil-public-runtime';
import { BaseInputCustomEvent } from '../../components';
export interface EditData {
  original: string;
  transcription?: string;
  translation: string;
  id: string;
}
export declare class AddWordDialog {
  original: string;
  transcription: string;
  translation: string;
  isOpen: boolean;
  isEdit: boolean;
  editData?: EditData;
  closeDialog: EventEmitter<{}>;
  handleClose: () => void;
  setValuesToDefault: () => void;
  createWord: () => void;
  editWord: () => void;
  handleChangeOriginal: (event: BaseInputCustomEvent<{
    value: string;
  }>) => void;
  handleChangeTranscription: (event: BaseInputCustomEvent<{
    value: string;
  }>) => void;
  handleChangeTranslation: (event: BaseInputCustomEvent<{
    value: string;
  }>) => void;
  componentWillRender(): void;
  render(): any;
}

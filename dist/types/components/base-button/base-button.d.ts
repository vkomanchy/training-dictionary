import { EventEmitter } from '../../stencil-public-runtime';
export declare class BaseButton {
  additionalStyle: {
    [key: string]: string;
  };
  isDisabled: boolean;
  buttonClick: EventEmitter<{}>;
  handleClick: (e: MouseEvent) => void;
  render(): any;
}

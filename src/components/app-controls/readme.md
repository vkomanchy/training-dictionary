# app-controls



<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [app-root](../app-root)

### Depends on

- [base-button](../base-button)
- context-consumer

### Graph
```mermaid
graph TD;
  app-controls --> base-button
  app-controls --> context-consumer
  base-button --> context-consumer
  app-root --> app-controls
  style app-controls fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

const DEFAULT_LANGUAGE = 'en';
const SUPPORTED_LANGUAGES = ['ru', 'en'];
const TRANSLATIONS = {
  ru: {
    switchToDarkTheme: 'Включить темную тему',
    switchToLightTheme: 'Включить светлую тему',
    addWord: 'Добавить слово',
    editWord: 'Изменить слово',
    repeatWords: 'Повторить слова',
    endRepeat: 'Закончить повторение слов',
    checkWords: 'Проверить слова',
    switchLanguage: 'Сменить язык',
    originalLabel: 'Оригинал',
    transcriptionLabel: 'Транскрипция',
    translationLabel: 'Перевод',
    addWordDialogHeading: 'Добавление слова',
    createWord: 'Добавить',
    closeDialog: 'Закрыть',
    confirmRepeat: 'Начать повтор',
    repeatSettingsDialogHeading: 'Настройки повтора слов',
    repeatWordsCountLabel: 'Кол-во слов',
    totalWords: 'Общее количество слов',
    originalTranslate: 'Обычный перевод',
    reverseTranslate: 'Обратный перевод',
    translateGuessLabel: 'Введите ваш перевод',
    acceptWarningButton: 'Да',
    warningDialogHeaderText: 'Предупреждение',
    deleteDialogWarning: 'Вы хотите удалить слово?'
  },
  en: {
    switchToDarkTheme: 'Switch to dark theme',
    switchToLightTheme: 'Switch to light theme',
    addWord: 'Add word',
    editWord: 'Edit word',
    repeatWords: 'Repeat words',
    endRepeat: 'End repeating words',
    checkWords: 'Check words',
    switchLanguage: 'Change language',
    originalLabel: 'Original',
    transcriptionLabel: 'Transcription',
    translationLabel: 'Translation',
    addWordDialogHeading: 'Adding word',
    createWord: 'Add',
    closeDialog: 'Close',
    confirmRepeat: 'Start repeating',
    repeatSettingsDialogHeading: 'Repeat words settings',
    totalWords: 'Total words count',
    repeatWordsCountLabel: 'Words count',
    originalTranslate: 'Original translate',
    reverseTranslate: 'Reverse translate',
    translateGuessLabel: 'Enter your translation',
    acceptWarningButton: 'Yes',
    warningDialogHeaderText: 'Warning',
    deleteDialogWarning: 'Do you want to delete word?'
  }
};
const initBrowserLang = () => {
  if (typeof window === 'undefined'
    || typeof window.navigator === 'undefined') {
    return undefined;
  }
  let browserLang = (window.navigator.languages && window.navigator.languages.length > 0)
    ? window.navigator.languages[0]
    : null;
  // @ts-ignore
  browserLang = browserLang || window.navigator.language || window.navigator.browserLanguage || window.navigator.userLanguage;
  if (typeof browserLang === 'undefined') {
    return undefined;
  }
  if (browserLang.indexOf('-') !== -1) {
    browserLang = browserLang.split('-')[0];
  }
  if (browserLang.indexOf('_') !== -1) {
    browserLang = browserLang.split('_')[0];
  }
  return browserLang;
};
const translate = (key, currentLang) => {
  const isLanguageKeyValid = currentLang !== undefined && SUPPORTED_LANGUAGES.includes(currentLang);
  return TRANSLATIONS[isLanguageKeyValid ? currentLang : DEFAULT_LANGUAGE][key];
};

export { initBrowserLang as i, translate as t };

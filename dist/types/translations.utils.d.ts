export declare const initBrowserLang: () => string | undefined;
export declare const translate: (key: string, currentLang: string) => string;

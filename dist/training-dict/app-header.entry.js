import { r as registerInstance, h, e as Host } from './index-a0476a79.js';
import { T as Tunnel } from './state-e1173bff.js';
import { t as translate } from './translations.utils-32242b75.js';

const Sun = `<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 512 512">
  <g>
    <g>
      <g>
        <path d="m179.4,367.8c38.4,0 61,24.2 76.6,55.6 15.6-31.4 38.3-55.6 76.6-55.6 12.7,0 26.7,2.6 43.3,8.2-9.3-27.5-11.6-51.6-2.3-73.3 8.9-20.8 27.4-35.4 50.1-46.7-22.7-11.3-41.2-25.9-50.1-46.7-9.3-21.6-7-45.7 2.3-73.3-16.7,5.6-30.6,8.2-43.3,8.2-38.4,0-61-24.2-76.6-55.6-15.6,31.4-38.3,55.6-76.6,55.6-12.7,0-26.6-2.6-43.3-8.2 9.3,27.5 11.6,51.6 2.3,73.3-8.9,20.8-27.4,35.4-50.1,46.7 22.7,11.3 41.2,25.9 50.1,46.7 9.3,21.6 7,45.7-2.3,73.3 16.6-5.6 30.6-8.2 43.3-8.2zm76.6,133.2c-8.9,0-16.8-5.8-19.5-14.3-20.5-65.3-36.4-78.1-57.1-78.1-16.1,0-39.4,7.8-73.1,24.5-7.9,3.9-17.3,2.3-23.5-3.9-6.2-6.2-7.7-15.6-3.9-23.5 21.9-44.3 28.7-71.2 21.9-87-6.7-15.7-30-29-75.5-43.3-8.5-2.6-14.3-10.5-14.3-19.4 0-8.9 5.8-16.8 14.3-19.5 45.5-14.3 68.8-27.6 75.5-43.3 6.8-15.8 0-42.7-21.9-87-3.9-7.8-2.3-17.2 3.9-23.4 6.2-6.2 15.6-7.7 23.5-3.9 33.8,16.7 57,24.5 73.1,24.5 16.5,0 34.6-6 57.1-78.1 2.7-8.5 10.6-14.3 19.5-14.3 8.9,0 16.8,5.8 19.5,14.3 20.5,65.3 36.4,78.1 57.1,78.1 16.1,0 39.4-7.8 73.1-24.5 7.9-3.9 17.3-2.3 23.5,3.9 6.2,6.2 7.7,15.6 3.9,23.5-21.9,44.3-28.7,71.2-21.9,87 6.7,15.7 30,29 75.5,43.3 8.5,2.7 14.3,10.6 14.3,19.5 0,8.9-5.8,16.8-14.3,19.5-45.5,14.3-68.8,27.6-75.5,43.3-6.8,15.8 0,42.7 21.9,87 3.9,7.8 2.3,17.3-3.9,23.5-6.2,6.2-15.6,7.8-23.5,3.9-33.8-16.7-57-24.5-73.1-24.5-16.5,0-34.6,6-57.1,78.1-2.7,8.3-10.6,14.1-19.5,14.1z"/>
      </g>
    </g>
  </g>
</svg>
`;

const Moon = `<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 172.151 172.151" style="enable-background:new 0 0 172.151 172.151;" xml:space="preserve">
<g>
	<g>
		<g>
			<path d="M94.992,27.9c-1.292-0.552-2.796-0.292-3.832,0.67c-1.031,0.963-1.398,2.443-0.937,3.777
				c8.126,23.446,1.654,49.526-16.488,66.441c-18.141,16.918-44.614,21.557-67.43,11.814c-1.297-0.55-2.8-0.291-3.832,0.672
				c-1.031,0.963-1.398,2.443-0.937,3.777c3.195,9.216,8.213,17.533,14.916,24.72c12.722,13.643,29.998,21.515,48.642,22.166
				c0.836,0.03,1.666,0.044,2.499,0.044c17.728,0,34.54-6.611,47.573-18.764c13.643-12.724,21.517-29.998,22.168-48.643
				c0.651-18.644-5.998-36.426-18.72-50.069C111.91,37.318,103.964,31.731,94.992,27.9z M130.205,94.325
				c-0.584,16.74-7.653,32.251-19.905,43.675c-12.25,11.423-28.237,17.373-44.957,16.811c-16.741-0.585-32.251-7.654-43.674-19.904
				c-4.208-4.513-7.678-9.519-10.363-14.948c23.46,6.862,49.127,0.987,67.294-15.954c18.165-16.939,25.826-42.128,20.608-66.018
				c5.229,3.059,9.982,6.869,14.188,11.382C124.821,61.619,130.79,77.586,130.205,94.325z"/>
			<path d="M47.4,31.377c0.696,0.696,1.609,1.045,2.521,1.045s1.825-0.348,2.521-1.045l5.107-5.107l5.107,5.107
				c0.696,0.696,1.609,1.045,2.521,1.045s1.825-0.348,2.521-1.045c1.393-1.393,1.393-3.649,0-5.042l-5.106-5.106l4.972-4.972
				c1.393-1.393,1.393-3.649,0-5.042c-1.393-1.393-3.649-1.393-5.042,0l-4.972,4.972l-4.973-4.973c-1.393-1.393-3.649-1.393-5.042,0
				c-1.393,1.393-1.393,3.649,0,5.042l4.973,4.973L47.4,26.335C46.007,27.728,46.007,29.984,47.4,31.377z"/>
			<path d="M171.107,65.559L166,60.452l4.973-4.973c1.393-1.393,1.393-3.649,0-5.042s-3.649-1.393-5.042,0l-4.973,4.973
				l-4.973-4.973c-1.393-1.393-3.649-1.393-5.042,0s-1.393,3.649,0,5.042l4.973,4.973l-5.107,5.107
				c-1.393,1.393-1.393,3.649,0,5.042c0.696,0.696,1.609,1.045,2.521,1.045s1.825-0.348,2.521-1.045l5.107-5.107l5.107,5.107
				c0.696,0.696,1.609,1.045,2.521,1.045s1.825-0.348,2.521-1.045C172.499,69.208,172.499,66.952,171.107,65.559z"/>
			<path d="M6.087,95.562l5.107-5.107l5.107,5.107c0.696,0.696,1.609,1.045,2.521,1.045c0.912,0,1.825-0.348,2.521-1.045
				c1.393-1.393,1.393-3.649,0-5.042l-5.107-5.107l4.973-4.973c1.393-1.393,1.393-3.649,0-5.042s-3.649-1.393-5.042,0l-4.973,4.973
				l-4.972-4.973c-1.393-1.393-3.649-1.393-5.042,0s-1.393,3.649,0,5.042l4.973,4.973l-5.108,5.106
				c-1.393,1.393-1.393,3.649,0,5.042c0.696,0.696,1.609,1.045,2.521,1.045C4.478,96.606,5.39,96.258,6.087,95.562z"/>
		</g>
	</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
`;

const getUserTheme = () => window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
  ? 'dark'
  : 'light';

const appHeaderCss = ".app-header{display:flex;height:50px;align-items:center;padding:0 16px;justify-content:space-between}.app-header-light{background-color:#EBEBF7}.app-header-dark{background-color:#1b1b1b}.app-heading{font-size:22px;font-family:'Finlandica', sans-serif;font-weight:bold}.app-heading-light{color:#1b1b1b}.app-heading-dark{color:#EBEBF7}.theme-switcher,.language-switcher{border:none;background:none;cursor:pointer}.icon-container{height:20px}.icon-container svg{height:20px}.dark-icon-container svg{fill:#EBEBF7}.buttons-block{display:flex;align-items:center;flex-basis:70px;justify-content:space-between;padding-right:20px}.light-word-container{color:#1b1b1b}.dark-word-container{color:#EBEBF7}";

const AppHeader = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.isInitialThemeSettled = false;
  }
  render() {
    return (h(Tunnel.Consumer, null, ({ theme, language, setTheme, changeTheme, changeLanguage }) => {
      if (!this.isInitialThemeSettled) {
        setTheme(getUserTheme());
      }
      this.isInitialThemeSettled = true;
      return (h(Host, null, h("header", { class: `app-header app-header-${theme}` }, h("h1", { class: `app-heading app-heading-${theme}` }, h("slot", null)), h("div", { class: 'buttons-block' }, h("div", { class: "theme-switcher-container", title: translate(theme === 'light' ? 'switchToDarkTheme' : 'switchToLightTheme', language) }, h("button", { onClick: changeTheme, class: "theme-switcher" }, theme === 'light'
        ? h("div", { class: 'icon-container light-icon-container', innerHTML: Moon })
        : h("div", { class: 'icon-container dark-icon-container', innerHTML: Sun }))), h("div", { class: "language-switcher-container", title: translate('switchLanguage', language) }, h("button", { onClick: changeLanguage, class: "language-switcher" }, language === 'ru'
        ? h("div", { class: `word-container ${theme}-word-container` }, "RU")
        : h("div", { class: `word-container ${theme}-word-container` }, "EN")))))));
    }));
  }
};
Tunnel.injectProps(AppHeader, ['theme', 'language']);
AppHeader.style = appHeaderCss;

export { AppHeader as app_header };

import { r as registerInstance, f as createEvent, h } from './index-a0476a79.js';
import { d as deleteWordFromStorage } from './localStorage.utils-64019c8a.js';
import { t as translate } from './translations.utils-32242b75.js';
import { T as Tunnel } from './state-e1173bff.js';

const dictionaryWordsCss = ".words-container{display:grid;margin-top:20px;padding:0 20px;gap:20px;grid-template-columns:repeat(6, 1fr)}";

const DictionaryWords = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.updateCards = createEvent(this, "updateCards", 7);
    this.deleteWordId = null;
    this.isDeleteDialogOpen = false;
    this.deleteCardCallback = () => {
      console.log('this.deleteWordId', this.deleteWordId);
      if (this.deleteWordId) {
        deleteWordFromStorage(this.deleteWordId);
        this.updateCards.emit();
      }
      this.handleCloseDeleteDialog();
    };
    this.handleDeleteCard = (e) => {
      this.deleteWordId = e.target.wordId;
      this.isDeleteDialogOpen = true;
    };
    this.handleCloseDeleteDialog = () => {
      this.isDeleteDialogOpen = false;
      this.deleteWordId = null;
    };
    this.handleEditCard = () => {
      this.updateCards.emit();
    };
  }
  render() {
    return (h(Tunnel.Consumer, null, ({ words, mode, repeatWordsCount, language }) => (h("div", { class: 'words-container' }, ((mode === 'repeatWords' || mode === 'reverseRepeatWords')
      ? words.slice(0, repeatWordsCount)
      : words).map(({ id, translation, transcription, original }) => (h("word-card", { wordId: id, translation: translation, transcription: transcription, original: original, onDeleteCard: this.handleDeleteCard, onEditCard: this.handleEditCard }))), this.isDeleteDialogOpen && (h("warning-dialog", { isOpen: this.isDeleteDialogOpen, warning: translate('deleteDialogWarning', language), onAcceptDialog: this.deleteCardCallback, onCloseDialog: this.handleCloseDeleteDialog }))))));
  }
};
DictionaryWords.style = dictionaryWordsCss;

export { DictionaryWords as dictionary_words };

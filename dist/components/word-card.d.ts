import type { Components, JSX } from "../types/components";

interface WordCard extends Components.WordCard, HTMLElement {}
export const WordCard: {
  prototype: WordCard;
  new (): WordCard;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;

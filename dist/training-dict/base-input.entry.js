import { r as registerInstance, f as createEvent, h, e as Host } from './index-a0476a79.js';

const baseInputCss = ":host{display:flex;flex-direction:column}label{margin-bottom:4px;font-family:'Open Sans', sans-serif}input{outline:none;height:22px;font-family:'Open Sans', sans-serif}";

const BaseInput = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.inputChange = createEvent(this, "inputChange", 7);
    this.additionalStyle = {};
    this.handleChange = (e) => {
      this.inputChange.emit({ value: e.target.value });
    };
  }
  render() {
    return (h(Host, { style: this.additionalStyle }, this.inputLabel && h("label", { htmlFor: this.inputId }, this.inputLabel), h("input", { type: 'text', id: this.inputId, placeholder: this.inputPlaceholder, value: this.value, onInput: this.handleChange })));
  }
};
BaseInput.style = baseInputCss;

export { BaseInput as base_input };

# word-card



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute       | Description | Type     | Default     |
| --------------- | --------------- | ----------- | -------- | ----------- |
| `original`      | `original`      |             | `string` | `undefined` |
| `transcription` | `transcription` |             | `string` | `undefined` |
| `translation`   | `translation`   |             | `string` | `undefined` |
| `wordId`        | `word-id`       |             | `string` | `undefined` |


## Dependencies

### Used by

 - [dictionary-words](../dictionary-words)

### Depends on

- context-consumer

### Graph
```mermaid
graph TD;
  word-card --> context-consumer
  dictionary-words --> word-card
  style word-card fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

import type { Components, JSX } from "../types/components";

interface BaseDialog extends Components.BaseDialog, HTMLElement {}
export const BaseDialog: {
  prototype: BaseDialog;
  new (): BaseDialog;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;

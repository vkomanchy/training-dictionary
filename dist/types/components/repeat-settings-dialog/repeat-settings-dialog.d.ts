import { EventEmitter } from '../../stencil-public-runtime';
export declare class RepeatSettingsDialog {
  isOpen: boolean;
  repeatWordsCount: number;
  selectedType: string;
  closeDialog: EventEmitter<{}>;
  handleClose: () => void;
  render(): any;
}

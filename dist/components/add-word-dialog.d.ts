import type { Components, JSX } from "../types/components";

interface AddWordDialog extends Components.AddWordDialog, HTMLElement {}
export const AddWordDialog: {
  prototype: AddWordDialog;
  new (): AddWordDialog;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;

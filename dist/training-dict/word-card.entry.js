import { r as registerInstance, f as createEvent, h, F as Fragment } from './index-a0476a79.js';
import { t as translate } from './translations.utils-32242b75.js';
import { T as Tunnel } from './state-e1173bff.js';
import { i as incrementWordMistake } from './localStorage.utils-64019c8a.js';

const Edit = `<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 488.728 488.728" style="enable-background:new 0 0 488.728 488.728;" xml:space="preserve">
<g>
	<path d="M487.147,462.52l-36.4-167.6c0-4.2-2.1-7.3-5.2-10.4l-261.3-261.3c-20-22.9-74.3-38.1-112.4,0l-47.9,47.9
		c-31,31-31,81.4,0,112.4l261.3,261.3c2.1,2.1,5.2,4.2,9.4,5.2l168.6,38.5C473.347,490.02,492.347,483.62,487.147,462.52z
		 M53.047,154.42c-15.6-15.6-15.6-39.6,0-55.2l47.9-47.9c15.2-15.2,40-15.2,55.2,0l238.4,238.4h-27.1c-11.4,0-20.8,9.4-20.8,20.8
		v34.3h-34.3c-11.4,0-20.8,9.4-20.8,20.8v26.1L53.047,154.42z M333.047,415.72v-29.2h34.3c18,1.7,20.8-16.5,20.8-20.8v-34.4h29.2
		l24,109.3L333.047,415.72z"/>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
`;

const Trash = `<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 458.5 458.5" style="enable-background:new 0 0 458.5 458.5;" xml:space="preserve">
<g>
	<g>
		<g>
			<path d="M382.078,57.069h-89.78C289.128,25.075,262.064,0,229.249,0S169.37,25.075,166.2,57.069H76.421
				c-26.938,0-48.854,21.916-48.854,48.854c0,26.125,20.613,47.524,46.429,48.793V399.5c0,32.533,26.467,59,59,59h192.508
				c32.533,0,59-26.467,59-59V154.717c25.816-1.269,46.429-22.668,46.429-48.793C430.933,78.985,409.017,57.069,382.078,57.069z
				 M229.249,30c16.244,0,29.807,11.673,32.76,27.069h-65.52C199.442,41.673,213.005,30,229.249,30z M354.503,399.501
				c0,15.991-13.009,29-29,29H132.995c-15.991,0-29-13.009-29-29V154.778c12.244,0,240.932,0,250.508,0V399.501z M382.078,124.778
				c-3.127,0-302.998,0-305.657,0c-10.396,0-18.854-8.458-18.854-18.854S66.025,87.07,76.421,87.07h305.657
				c10.396,0,18.854,8.458,18.854,18.854S392.475,124.778,382.078,124.778z"/>
			<path d="M229.249,392.323c8.284,0,15-6.716,15-15V203.618c0-8.284-6.715-15-15-15c-8.284,0-15,6.716-15,15v173.705
				C214.249,385.607,220.965,392.323,229.249,392.323z"/>
			<path d="M306.671,392.323c8.284,0,15-6.716,15-15V203.618c0-8.284-6.716-15-15-15s-15,6.716-15,15v173.705
				C291.671,385.607,298.387,392.323,306.671,392.323z"/>
			<path d="M151.828,392.323c8.284,0,15-6.716,15-15V203.618c0-8.284-6.716-15-15-15c-8.284,0-15,6.716-15,15v173.705
				C136.828,385.607,143.544,392.323,151.828,392.323z"/>
		</g>
	</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
`;

const wordCardCss = ".word-card-container{display:flex}.word-card-container{border-radius:5px;display:flex;flex-direction:column;padding:10px 10px 14px 10px;box-shadow:0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23)}.light-card-container{background:#ebebf7}.dark-card-container{background-color:#1b1b1b}.words-data p{margin:0;font-size:14px}.words-data{display:grid;grid-template-columns:1fr 2fr;padding:10px;row-gap:8px;column-gap:8px}.word-card-header{display:flex;justify-content:flex-end;padding:5px 10px 0 0}.word-card-button{cursor:pointer;background:none;border:none;outline:none}.word-icon-container{height:14px}.word-icon-container svg{height:14px}.dark-word-icon-container svg{fill:#EBEBF7}.word-text,.word-label{font-family:'Open Sans', sans-serif}.light-word-text,.light-word-label{color:rgb(51, 53, 55)}.dark-word-text,.dark-word-label{color:white}.is-wrong{color:red;text-decoration:line-through}.is-right{color:green}";

const WordCard = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.deleteCard = createEvent(this, "deleteCard", 7);
    this.editCard = createEvent(this, "editCard", 7);
    this.translateGuess = '';
    this.guessType = 'repeatWords';
    this.handleCloseEdit = () => {
      this.isOpen = false;
      this.editCard.emit();
    };
    this.handleClickEdit = () => {
      this.isOpen = true;
    };
    this.handleClickDelete = () => {
      this.deleteCard.emit({ wordId: this.wordId });
    };
    this.handleChangeTranslationGuess = (event) => {
      const { detail: { value } } = event;
      this.translateGuess = value;
    };
  }
  render() {
    return (h(Tunnel.Consumer, null, ({ mode, theme, language }) => {
      if (mode === 'repeatWords' || mode === 'reverseRepeatWords') {
        this.guessType = mode;
      }
      if (mode === 'view') {
        this.translateGuess = '';
        return (h("div", { class: `word-card-container view-mode ${theme}-card-container` }, h("div", { class: `word-card-header ${theme}-card-header` }, h("button", { class: 'word-card-button', onClick: this.handleClickEdit }, h("div", { class: `word-icon-container ${theme}-word-icon-container`, innerHTML: Edit })), h("button", { class: 'word-card-button', onClick: this.handleClickDelete }, h("div", { class: `word-icon-container ${theme}-word-icon-container`, innerHTML: Trash }))), h("div", { class: `words-data ${theme}-words-data` }, h("p", { class: `word-label ${theme}-word-label` }, translate('originalLabel', language)), h("p", { class: `word-text ${theme}-word-text` }, this.original), this.transcription && (h(Fragment, null, h("p", { class: `word-label ${theme}-word-label` }, translate('transcriptionLabel', language)), h("p", { class: `word-text ${theme}-word-text` }, this.transcription))), h("p", { class: `word-label ${theme}-word-label` }, translate('translationLabel', language)), h("p", { class: `word-text ${theme}-word-text` }, this.translation)), h("add-word-dialog", { isOpen: this.isOpen, isEdit: true, editData: {
            original: this.original,
            transcription: this.transcription,
            translation: this.translation,
            id: this.wordId,
          }, onCloseDialog: this.handleCloseEdit })));
      }
      if (mode === 'repeatWords' || mode === 'reverseRepeatWords') {
        return (h("div", { class: `word-card-container view-mode ${theme}-card-container` }, h("div", { class: `words-data ${theme}-words-data` }, mode === 'reverseRepeatWords'
          ? (h(Fragment, null, h("base-input", { value: this.translateGuess, onInputChange: this.handleChangeTranslationGuess, inputId: 'translateGuess', inputPlaceholder: translate('translateGuessLabel', language) }), h("div", { class: 'empty-block' })))
          : (h(Fragment, null, h("p", { class: `word-label ${theme}-word-label` }, translate('originalLabel', language)), h("p", { class: `word-text ${theme}-word-text` }, this.original))), this.transcription && (h(Fragment, null, h("p", { class: `word-label ${theme}-word-label` }, translate('transcriptionLabel', language)), h("p", { class: `word-text ${theme}-word-text` }, this.transcription))), mode === 'repeatWords'
          ? (h("base-input", { value: this.translateGuess, onInputChange: this.handleChangeTranslationGuess, inputId: 'translateGuess', inputPlaceholder: translate('translateGuessLabel', language) }))
          : (h(Fragment, null, h("p", { class: `word-label ${theme}-word-label` }, translate('translationLabel', language)), h("p", { class: `word-text ${theme}-word-text` }, this.translation))))));
      }
      if (mode === 'checkWords') {
        const isCheckBasicTranslation = this.guessType === 'repeatWords';
        const lowerCasedTranslation = this.translation.toLowerCase();
        const lowerCasedOriginal = this.original.toLowerCase();
        const lowerCasedTranslateGuess = this.translateGuess.toLowerCase().trim();
        const isWordCorrect = isCheckBasicTranslation
          ? lowerCasedTranslation === lowerCasedTranslateGuess
          : lowerCasedOriginal === lowerCasedTranslateGuess;
        if (!isWordCorrect) {
          incrementWordMistake(this.wordId);
        }
        const originalClassName = (() => {
          if (!isCheckBasicTranslation) {
            return `word-text ${theme}-word-text ${isWordCorrect ? 'is-right' : 'is-wrong'}`;
          }
          return `word-text ${theme}-word-text`;
        })();
        const translationClassName = (() => {
          if (isCheckBasicTranslation) {
            return `word-text ${theme}-word-text ${isWordCorrect ? 'is-right' : 'is-wrong'}`;
          }
          return `word-text ${theme}-word-text`;
        })();
        return (h("div", { class: `word-card-container view-mode ${theme}-card-container` }, h("div", { class: `words-data ${theme}-words-data` }, h("p", { class: `word-label ${theme}-word-label` }, translate('originalLabel', language)), h("p", { class: originalClassName }, !isCheckBasicTranslation ? this.translateGuess : this.original), !isWordCorrect && !isCheckBasicTranslation && (h(Fragment, null, h("div", { class: 'empty-block' }), h("p", { class: 'word-text is-right' }, this.original))), this.transcription && (h(Fragment, null, h("p", { class: `word-label ${theme}-word-label` }, translate('transcriptionLabel', language)), h("p", { class: `word-text ${theme}-word-text` }, this.transcription))), h("p", { class: `word-label ${theme}-word-label` }, translate('translationLabel', language)), h("p", { class: translationClassName }, isCheckBasicTranslation ? this.translateGuess : this.translation), !isWordCorrect && isCheckBasicTranslation && (h(Fragment, null, h("div", { class: 'empty-block' }), h("p", { class: 'word-text is-right' }, this.translation))))));
      }
    }));
  }
};
WordCard.style = wordCardCss;

export { WordCard as word_card };

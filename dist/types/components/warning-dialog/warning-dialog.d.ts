import { EventEmitter } from '../../stencil-public-runtime';
export declare class WarningDialog {
  isOpen: boolean;
  warning: string;
  acceptDialog: EventEmitter<{}>;
  closeDialog: EventEmitter<{}>;
  handleClose: () => void;
  handleConfirm: () => void;
  render(): any;
}

# base-button



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute | Description | Type                         | Default |
| ----------------- | --------- | ----------- | ---------------------------- | ------- |
| `additionalStyle` | --        |             | `{ [key: string]: string; }` | `{}`    |


## Events

| Event         | Description | Type              |
| ------------- | ----------- | ----------------- |
| `buttonClick` |             | `CustomEvent<{}>` |


## Dependencies

### Used by

 - [app-controls](../app-controls)

### Depends on

- context-consumer

### Graph
```mermaid
graph TD;
  base-button --> context-consumer
  app-controls --> base-button
  style base-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

import { Component, Host, h, Prop, EventEmitter, Event } from '@stencil/core';
import Tunnel, { State } from '../data/state';
import { translate } from '../../translations.utils';

export interface DialogAction {
  onClick: () => void;
  text: string;
  isDisabled?: boolean;
};

@Component({
  tag: 'base-dialog',
  styleUrl: 'base-dialog.css',
  shadow: true,
})
export class BaseDialog {
  @Prop() isOpen: boolean;
  @Prop() actions: DialogAction[];

  @Event({
    eventName: 'closeDialog',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) closeDialog: EventEmitter<{}>;

  handleClickOutside = (e) => {
    if (e.target.className === 'dialog') {
      this.closeDialog.emit();
    }
  };

  handleClose = () => {
    this.closeDialog.emit();
  };

  render() {
    return (
      <Tunnel.Consumer>
        {({ language }: State) => (
          <Host>
            <div class="dialog" onClick={this.handleClickOutside}>
              <div class="dialog-inner">
                <div class="dialog-header">
                  <slot name='headerText'></slot>
                </div>

                <div class='dialog-body'>
                  <slot></slot>
                </div>

                <div class='dialog-footer'>
                  {
                    [
                      ...this.actions,
                      {
                        onClick: this.handleClose,
                        text: translate('closeDialog', language)
                      },
                    ]
                      .map(({ onClick, text, isDisabled }) => (
                        <base-button onButtonClick={onClick} additionalStyle={{ marginLeft: '8px' }} isDisabled={isDisabled}>
                          {text}
                        </base-button>
                      ))}
                </div>
              </div>
            </div>
          </Host>
        )}
      </Tunnel.Consumer>
    );
  }
}

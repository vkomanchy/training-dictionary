import type { Components, JSX } from "../types/components";

interface RepeatSettingsDialog extends Components.RepeatSettingsDialog, HTMLElement {}
export const RepeatSettingsDialog: {
  prototype: RepeatSettingsDialog;
  new (): RepeatSettingsDialog;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;

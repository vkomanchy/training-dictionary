import type { Components, JSX } from "../types/components";

interface WarningDialog extends Components.WarningDialog, HTMLElement {}
export const WarningDialog: {
  prototype: WarningDialog;
  new (): WarningDialog;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;

export interface Word {
  original: string;
  transcription?: string;
  translation: string;
  id: string;
}

const WORDS_KEY = 'words';

const MISTAKES_KEY = 'mistakes';

export const getWordsFromLocalStorage = (): Word[] | undefined => {
  const words = localStorage.getItem(WORDS_KEY);

  if (words) {
    return JSON.parse(words);
  }

  return;
};

export const writeWordToLocalStorage = (word: Word) => {
  if (window.localStorage) {
    const { localStorage } = window;

    const words = localStorage.getItem(WORDS_KEY);

    if (words) {
      const wordsArray = JSON.parse(words);

      localStorage.setItem(WORDS_KEY, JSON.stringify([...wordsArray, word]));
    } else {
      localStorage.setItem(WORDS_KEY, JSON.stringify([word]));
    }
  } else {
    throw new Error('localStorage is not defined. Sorry, you can use this app!');
  }
};

export const deleteWordFromStorage = (id: string) => {
  if (window.localStorage) {
    const { localStorage } = window;

    const words = localStorage.getItem(WORDS_KEY);

    if (words) {
      const wordsArray = JSON.parse(words);

      const newWordsArray = wordsArray.filter(({ id: wordId }) => wordId !== id);

      if (wordsArray.length === newWordsArray.length) {
        console.error('Word was not found');
      } else {
        localStorage.setItem(WORDS_KEY, JSON.stringify(newWordsArray));
      }
    } else {
      console.error('Words list is empty');
    }
  } else {
    throw new Error('localStorage is not defined. Sorry, you can use this app!');
  }
};

export const editWordInLocalStorage = (word: Word) => {
  if (window.localStorage) {
    const { localStorage } = window;

    const { id } = word;

    const words = localStorage.getItem(WORDS_KEY);

    if (words) {
      const wordsArray = JSON.parse(words);

      const foundWordIndex = wordsArray.findIndex(({ id: wordId }) => wordId === id);

      if (foundWordIndex !== -1) {
        wordsArray[foundWordIndex] = word;

        localStorage.setItem(WORDS_KEY, JSON.stringify(wordsArray));
      } else {
        console.error('Error was not found');
      }
    } else {
      console.error('Words list is empty');
    }
  } else {
    throw new Error('localStorage is not defined. Sorry, you can use this app!');
  }
};

export const dropStorage = () => {
  localStorage.clear();
};

export const incrementWordMistake = (id: string) => {
  const mistakes = localStorage.getItem(MISTAKES_KEY);

  if (mistakes) {
    const mistakesObject = JSON.parse(mistakes);

    if (mistakesObject[id]) {
      mistakesObject[id] += 1;
    } else {
      mistakesObject[id] = 1;
    }

    localStorage.setItem(MISTAKES_KEY, JSON.stringify(mistakesObject));
  } else {
    const newMistakes = {
      [id]: 1,
    };

    localStorage.setItem(MISTAKES_KEY, JSON.stringify(newMistakes));
  }
};

export const getMistakesStat = (): { [key: string]: number } => {
  const mistakes = localStorage.getItem(MISTAKES_KEY);

  if (mistakes) {
    return JSON.parse(mistakes);
  } else {
    console.log('Mistakes is empty!');

    return {};
  }
};

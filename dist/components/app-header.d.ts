import type { Components, JSX } from "../types/components";

interface AppHeader extends Components.AppHeader, HTMLElement {}
export const AppHeader: {
  prototype: AppHeader;
  new (): AppHeader;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;

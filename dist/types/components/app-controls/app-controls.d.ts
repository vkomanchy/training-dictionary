import { EventEmitter } from '../../stencil-public-runtime';
export declare class AppControls {
  openAddWordDialog: EventEmitter<{}>;
  openRepeatSettingsDialog: EventEmitter<{}>;
  handleClickAddWord: () => void;
  handleClickRepeatWords: () => void;
  render(): any;
}

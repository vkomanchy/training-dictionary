import { r as registerInstance, f as createEvent, h } from './index-a0476a79.js';
import { t as translate } from './translations.utils-32242b75.js';
import { T as Tunnel } from './state-e1173bff.js';

const warningDialogCss = ":host{display:block}.warning-dialog-body{font-family:'Open Sans', sans-serif;font-size:18px;padding:10px}.warning-dialog-header-text{font-family:'Finlandica', sans-serif}";

const WarningDialog = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.acceptDialog = createEvent(this, "acceptDialog", 7);
    this.closeDialog = createEvent(this, "closeDialog", 7);
    this.isOpen = false;
    this.warning = '';
    this.handleClose = () => {
      this.closeDialog.emit();
    };
    this.handleConfirm = () => {
      this.acceptDialog.emit();
    };
  }
  render() {
    return (h(Tunnel.Consumer, null, ({ language }) => (h("base-dialog", { onCloseDialog: this.handleClose, isOpen: this.isOpen, actions: [
        {
          onClick: this.handleConfirm,
          text: translate('acceptWarningButton', language),
        },
      ] }, h("h3", { class: 'warning-dialog-header-text', slot: 'headerText' }, translate('warningDialogHeaderText', language)), h("div", { class: 'warning-dialog-body' }, this.warning)))));
  }
};
WarningDialog.style = warningDialogCss;

export { WarningDialog as warning_dialog };

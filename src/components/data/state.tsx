import { createProviderConsumer } from '@stencil/state-tunnel';
import { h } from '@stencil/core';
import { Word } from '../../localStorage.utils';

export interface State {
  theme: string;
  language: string;

  changeLanguage: () => void;
  changeTheme: () => void;

  _changeWords: (words: Word[]) => void;
  changeMode: (newMode: string) => void;
  changeRepeatWordsCount: (wordsCount: number) => void;
  setTheme: (theme: 'light' | 'dark') => void;
  sortWordsByStats: () => void;

  mode: string;

  words: Word[];
  
  repeatWordsCount: number;
}

export default createProviderConsumer<State>
  (
    {
      theme: 'light',
      language: 'en',

      changeLanguage: () => {},
      changeTheme: () => {},
      setTheme: () => {},

      _changeWords: () => {},
      changeMode: () => {},
      changeRepeatWordsCount: () => {},
      sortWordsByStats: () => {},

      mode: 'view',

      words: [],

      repeatWordsCount: 0,
    },
    (subscribe, child) => (
      <context-consumer subscribe={subscribe} renderer={child} />
    ),
  );

import { EventEmitter } from '../../stencil-public-runtime';
export declare class BaseInput {
  inputChange: EventEmitter<{
    value: string;
  }>;
  value: string;
  inputId: string;
  inputLabel: string;
  inputPlaceholder: string;
  additionalStyle: {
    [key: string]: string;
  };
  handleChange: (e: KeyboardEvent) => void;
  render(): any;
}

import { Component, Event, EventEmitter, h, State } from '@stencil/core';
import { WordCardCustomEvent } from '../../components';
import { deleteWordFromStorage } from '../../localStorage.utils';
import { translate } from '../../translations.utils';
import Tunnel, { State as TunnelState } from '../data/state';

@Component({
  tag: 'dictionary-words',
  styleUrl: 'dictionary-words.css',
  shadow: true,
})
export class DictionaryWords {
  @State() deleteWordId: string | null = null;
  @State() isDeleteDialogOpen = false;

  @Event({
    eventName: 'updateCards',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) updateCards: EventEmitter<{}>;

  deleteCardCallback = () => {
    console.log('this.deleteWordId', this.deleteWordId);

    if (this.deleteWordId) {
      deleteWordFromStorage(this.deleteWordId);

      this.updateCards.emit();
    }

    this.handleCloseDeleteDialog();
  }

  handleDeleteCard = (e: WordCardCustomEvent<{ wordId: string; }>) => {
    this.deleteWordId = e.target.wordId;
    this.isDeleteDialogOpen = true;
  }

  handleCloseDeleteDialog = () => {
    this.isDeleteDialogOpen = false;
    this.deleteWordId = null;
  }

  handleEditCard = () => {
    this.updateCards.emit();
  }

  render() {
    return (
      <Tunnel.Consumer>
        {({ words, mode, repeatWordsCount, language }: TunnelState) => (
          <div class='words-container'>
            {((mode === 'repeatWords' || mode === 'reverseRepeatWords')
              ? words.slice(0, repeatWordsCount)
              : words).map(({ id, translation, transcription, original }) => (
                <word-card
                  wordId={id}
                  translation={translation}
                  transcription={transcription}
                  original={original}
                  onDeleteCard={this.handleDeleteCard}
                  onEditCard={this.handleEditCard}
                />
              ))}

            {this.isDeleteDialogOpen && (
              <warning-dialog
                isOpen={this.isDeleteDialogOpen}
                warning={translate('deleteDialogWarning', language)}
                onAcceptDialog={this.deleteCardCallback}
                onCloseDialog={this.handleCloseDeleteDialog}
              />
            )}
          </div>
        )}
      </Tunnel.Consumer>
    );
  }
}

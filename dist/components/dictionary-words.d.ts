import type { Components, JSX } from "../types/components";

interface DictionaryWords extends Components.DictionaryWords, HTMLElement {}
export const DictionaryWords: {
  prototype: DictionaryWords;
  new (): DictionaryWords;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;

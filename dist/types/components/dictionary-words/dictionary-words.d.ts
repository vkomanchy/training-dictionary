import { EventEmitter } from '../../stencil-public-runtime';
import { WordCardCustomEvent } from '../../components';
export declare class DictionaryWords {
  deleteWordId: string | null;
  isDeleteDialogOpen: boolean;
  updateCards: EventEmitter<{}>;
  deleteCardCallback: () => void;
  handleDeleteCard: (e: WordCardCustomEvent<{
    wordId: string;
  }>) => void;
  handleCloseDeleteDialog: () => void;
  handleEditCard: () => void;
  render(): any;
}

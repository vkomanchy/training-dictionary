import { Component, EventEmitter, h, Event, Fragment } from '@stencil/core';
import Tunnel, { State } from '../data/state';

import { translate } from '../../translations.utils';

@Component({
  tag: 'app-controls',
  styleUrl: 'app-controls.css',
  shadow: true,
})
export class AppControls {
  @Event({
    eventName: 'openAddWordDialog',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) openAddWordDialog: EventEmitter<{}>;

  @Event({
    eventName: 'openRepeatSettingsDialog',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) openRepeatSettingsDialog: EventEmitter<{}>;

  handleClickAddWord = () => {
    this.openAddWordDialog.emit();
  };

  handleClickRepeatWords = () => {
    this.openRepeatSettingsDialog.emit();
  };

  render() {
    return (
      <Tunnel.Consumer>
        {({ language, mode, changeMode }: State) => {
          const handleCheckWords = () => {
            changeMode('checkWords');
          };

          const handleStopRepeating = () => {
            changeMode('view');
          };

          return (
            <div class="app-controls-container">
              {mode === 'view' && (
                <Fragment>
                  <base-button onClick={this.handleClickAddWord} additionalStyle={{ margin: '0 8px 0 18px' }}>{translate('addWord', language)}</base-button>

                  <base-button onClick={this.handleClickRepeatWords}>{translate('repeatWords', language)}</base-button>
                </Fragment>
              )}

              {(mode === 'repeatWords' || mode === 'reverseRepeatWords') && (
                <Fragment>
                  <base-button onClick={handleCheckWords} additionalStyle={{ margin: '0 8px 0 18px' }}>{translate('checkWords', language)}</base-button>

                  <base-button onClick={handleStopRepeating}>{translate('endRepeat', language)}</base-button>
                </Fragment>
              )}

              {mode === 'checkWords' && (
                <base-button onClick={handleStopRepeating} additionalStyle={{ margin: '0 8px 0 18px' }}>{translate('endRepeat', language)}</base-button>
              )}
            </div>
          );
        }}
      </Tunnel.Consumer>
    );
  }
}

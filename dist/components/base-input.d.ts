import type { Components, JSX } from "../types/components";

interface BaseInput extends Components.BaseInput, HTMLElement {}
export const BaseInput: {
  prototype: BaseInput;
  new (): BaseInput;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;

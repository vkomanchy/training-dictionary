# dictionary-words



<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [app-root](../app-root)

### Depends on

- [word-card](../word-card)
- context-consumer

### Graph
```mermaid
graph TD;
  dictionary-words --> word-card
  dictionary-words --> context-consumer
  word-card --> context-consumer
  app-root --> dictionary-words
  style dictionary-words fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

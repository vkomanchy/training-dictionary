import { Component, h, Prop, Event, EventEmitter, State } from '@stencil/core';
import { BaseInputCustomEvent } from '../../components';
import { translate } from '../../translations.utils';
import Tunnel, { State as TunnelState } from '../data/state';

@Component({
  tag: 'repeat-settings-dialog',
  styleUrl: 'repeat-settings-dialog.css',
  shadow: false,
})
export class RepeatSettingsDialog {
  @Prop() isOpen: boolean;

  @State() repeatWordsCount: number = 1;
  @State() selectedType = 'originalTranslate';

  @Event({
    eventName: 'closeDialog',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) closeDialog: EventEmitter<{}>;

  handleClose = () => {
    this.closeDialog.emit();
  }

  render() {
    if (this.isOpen) {
      return (
        <Tunnel.Consumer>
          {({ language, words, changeRepeatWordsCount, changeMode, sortWordsByStats }: TunnelState) => {
            const repeatTypeOptions = [
              {
                label: translate('originalTranslate', language),
                value: 'originalTranslate',
              },
              {
                label: translate('reverseTranslate', language),
                value: 'reverseTranslate',
              }
            ];

            const handleChangeWordsCount = (event: BaseInputCustomEvent<{ value: string }>) => {
              const { detail: { value } } = event;

              this.repeatWordsCount = Number(value);
            };

            const handleChangeRepeatType = (value) => () => {
              this.selectedType = value;
            }

            const handleConfirmRepeat = () => {
              if (this.selectedType === 'originalTranslate') {
                changeMode('repeatWords');
              } else {
                changeMode('reverseRepeatWords');
              }

              sortWordsByStats();

              changeRepeatWordsCount(this.repeatWordsCount);
              this.handleClose();
            };

            return (
              <base-dialog
                onCloseDialog={this.handleClose}
                isOpen={this.isOpen}
                actions={[
                  {
                    onClick: handleConfirmRepeat,
                    text: translate('confirmRepeat', language),
                    isDisabled: this.repeatWordsCount > words.length || this.repeatWordsCount <= 0 || isNaN(this.repeatWordsCount),
                  },
                ]}
              >
                <h3 class='repeat-settings-header-text' slot='headerText'>
                  {translate('repeatSettingsDialogHeading', language)}
                </h3>

                <div class='repeat-settings-dialog-body'>
                  <div class='repeat-type-choose'>
                    {repeatTypeOptions.map(({ label, value }) => (
                      <div class='repeat-type-option'>
                        <label htmlFor={value}>{label}</label>
                        <input
                          type='radio'
                          id={value}
                          value={String(this.selectedType === value)}
                          checked={this.selectedType === value}
                          onChange={handleChangeRepeatType(value)}
                        />
                      </div>
                    ))}
                  </div>

                  <base-input
                    value={String(this.repeatWordsCount)}
                    inputId='repeatWordsCount'
                    inputLabel={translate('repeatWordsCountLabel', language)}
                    onInputChange={handleChangeWordsCount}
                  />

                  <div class='repeat-stats-container'>
                    <div class='repeat-stat'>
                      <p class='stat-label'>{translate('totalWords', language)} - {words.length}</p>
                    </div>
                  </div>
                </div>
              </base-dialog>
            );
          }}
        </Tunnel.Consumer>
      );
    }

    return null;
  }
}

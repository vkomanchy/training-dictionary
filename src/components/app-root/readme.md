# app-root



<!-- Auto Generated Below -->


## Dependencies

### Depends on

- [app-header](../app-header)
- [app-controls](../app-controls)
- [dictionary-words](../dictionary-words)
- context-consumer

### Graph
```mermaid
graph TD;
  app-root --> app-header
  app-root --> app-controls
  app-root --> dictionary-words
  app-root --> context-consumer
  app-header --> context-consumer
  app-controls --> base-button
  app-controls --> context-consumer
  base-button --> context-consumer
  dictionary-words --> word-card
  dictionary-words --> context-consumer
  word-card --> context-consumer
  style app-root fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

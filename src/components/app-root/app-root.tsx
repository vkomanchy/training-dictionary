import { Component, h, Host, State } from '@stencil/core';
import { getMistakesStat, getWordsFromLocalStorage, Word } from '../../localStorage.utils';
import { initBrowserLang } from '../../translations.utils';
import Tunnel from '../data/state';
@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css',
  shadow: true,
})
export class AppRoot {
  @State() language = initBrowserLang();
  @State() theme = 'light';
  @State() words = [];
  @State() mode = 'view';

  @State() isAddWordDialogOpen = false;
  @State() isRepeatSettingsDialogOpen = false;

  @State() repeatWordsCount = 0;

  changeTheme = () => {
    if (this.theme === 'light') {
      this.theme = 'dark';
    } else {
      this.theme = 'light';
    }
  }

  setTheme = (theme: 'light' | 'dark') => {
    this.theme = theme;
  }

  changeLanguage = () => {
    if (this.language === 'ru') {
      this.language = 'en';
    } else {
      this.language = 'ru';
    }
  }

  changeMode = (newMode: string) => {
    this.mode = newMode;
  }

  _changeWords = (words: Word[]) => {
    this.words = words;
  }

  _handleOpenAddWordDialog = () => {
    this.isAddWordDialogOpen = true;
  }

  _handleCloseAddWordDialog = () => {
    this.isAddWordDialogOpen = false;

    this.fetchWords();
  }

  _handleOpenRepeatSettingsDialog = () => {
    this.isRepeatSettingsDialogOpen = true;
  }

  _handleCloseRepeatSettingsDialog = () => {
    this.isRepeatSettingsDialogOpen = false;
  }

  fetchWords = () => {
    const words = getWordsFromLocalStorage();

    this._changeWords(words);
  }

  sortWordsByStats = () => {
    const mistakes: { [key: string]: number } = getMistakesStat();

    (this.words as Word[]).sort((a, b) => {
      if (mistakes[a.id] && mistakes[b.id]) {
        const aMistakes = mistakes[a.id];
        const bMistakes = mistakes[b.id];

        return bMistakes - aMistakes;
      }

      if (mistakes[a.id] && !mistakes[b.id]) {
        return -1;
      }

      if (!mistakes[a.id] && mistakes[b.id]) {
        return 1;
      }

      return 0;
    })
  }

  changeRepeatWordsCount = (wordsCount: number) => {
    this.repeatWordsCount = wordsCount;
  }

  componentWillLoad() {
    this.fetchWords();
  }

  render() {
    const state = {
      theme: this.theme,
      language: this.language,

      words: this.words,
      mode: this.mode,

      repeatWordsCount: this.repeatWordsCount,

      changeTheme: this.changeTheme,
      setTheme: this.setTheme,
      changeLanguage: this.changeLanguage,

      sortWordsByStats: this.sortWordsByStats,

      changeMode: this.changeMode,
      changeRepeatWordsCount: this.changeRepeatWordsCount,

      _changeWords: this._changeWords,

      fetchWords: this.fetchWords,
    };

    return (
      <Host class={`root-${this.theme}`}>
        <Tunnel.Provider state={state}>
          <app-header>Training Dictionary</app-header>

          <app-controls
            onOpenAddWordDialog={this._handleOpenAddWordDialog}
            onOpenRepeatSettingsDialog={this._handleOpenRepeatSettingsDialog}
          />

          <dictionary-words
            onUpdateCards={this.fetchWords}
          />

          <add-word-dialog
            isOpen={this.isAddWordDialogOpen}
            onCloseDialog={this._handleCloseAddWordDialog}
          />

          <repeat-settings-dialog
            isOpen={this.isRepeatSettingsDialogOpen}
            onCloseDialog={this._handleCloseRepeatSettingsDialog}
          />
        </Tunnel.Provider>
      </Host>
    );
  }
}

import { Component, Host, h, Prop, Event, EventEmitter } from '@stencil/core';
import Tunnel, { State } from '../data/state';

@Component({
  tag: 'base-button',
  styleUrl: 'base-button.css',
  shadow: true,
})
export class BaseButton {
  @Prop() additionalStyle: { [key: string]: string; } = {};
  @Prop() isDisabled = false;

  @Event({
    eventName: 'buttonClick',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) buttonClick: EventEmitter<{}>;

  handleClick = (e: MouseEvent) => {
    this.buttonClick.emit(e);
  };

  render() {
    return (
      <Tunnel.Consumer>
        {({ theme }: State) => (
          <Host>
            <button
              onClick={this.handleClick }
              disabled={this.isDisabled}
              class={`button-container ${theme}-button-container button-${this.isDisabled ? 'disabled' : 'enabled'}`}
              style={this.additionalStyle}
            >
              <slot></slot>
            </button>
          </Host>
        )}
      </Tunnel.Consumer>
    );
  }
}

import { Component, h, Prop, Event, EventEmitter } from '@stencil/core';
import { translate } from '../../translations.utils';
import Tunnel, { State as TunnelState } from '../data/state';

@Component({
  tag: 'warning-dialog',
  styleUrl: 'warning-dialog.css',
  shadow: true,
})
export class WarningDialog {
  @Prop() isOpen = false;
  @Prop() warning: string = '';

  @Event({
    eventName: 'acceptDialog',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) acceptDialog: EventEmitter<{}>;

  @Event({
    eventName: 'closeDialog',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) closeDialog: EventEmitter<{}>;

  handleClose = () => {
    this.closeDialog.emit();
  }

  handleConfirm = () => {
    this.acceptDialog.emit();
  }

  render() {
    return (
      <Tunnel.Consumer>
        {({ language }: TunnelState) => (
          <base-dialog
            onCloseDialog={this.handleClose}
            isOpen={this.isOpen}
            actions={[
              {
                onClick: this.handleConfirm,
                text: translate('acceptWarningButton', language),
              },
            ]}
          >
            <h3 class='warning-dialog-header-text' slot='headerText'>
              {translate('warningDialogHeaderText', language)}
            </h3>

            <div class='warning-dialog-body'>
              {this.warning} 
            </div>
          </base-dialog>
        )}
      </Tunnel.Consumer>
    );
  }

}

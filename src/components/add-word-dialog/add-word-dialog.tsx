import { Component, Event, EventEmitter, h, Prop, State } from '@stencil/core';
import { v4 as uuidv4 } from 'uuid';
import Tunnel, { State as TunnelState } from '../data/state';
import { editWordInLocalStorage, writeWordToLocalStorage } from '../../localStorage.utils';
import { translate } from '../../translations.utils';
import { BaseInputCustomEvent } from '../../components';

export interface EditData {
  original: string;
  transcription?: string;
  translation: string;
  id: string;
}
@Component({
  tag: 'add-word-dialog',
  styleUrl: 'add-word-dialog.css',
  shadow: true,
})
export class AddWordDialog {
  @State() original: string;
  @State() transcription: string;
  @State() translation: string;

  @Prop() isOpen: boolean;
  @Prop() isEdit: boolean;

  @Prop() editData?: EditData;

  @Event({
    eventName: 'closeDialog',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) closeDialog: EventEmitter<{}>;

  handleClose = () => {
    this.closeDialog.emit();

    this.setValuesToDefault();
  };

  setValuesToDefault = () => {
    this.original = '';
    this.transcription = '';
    this.translation = '';
  };

  createWord = () => {
    const id = uuidv4();

    writeWordToLocalStorage({
      original: this.original.trim(),
      transcription: (this.transcription)?.trim(),
      translation: this.translation.trim(),
      id,
    });

    this.handleClose();
  }

  editWord = () => {
    editWordInLocalStorage({
      original: this.original.trim(),
      transcription: (this.transcription)?.trim(),
      translation: this.translation.trim(),
      id: this.editData.id,
    });

    this.handleClose();
  };

  handleChangeOriginal = (event: BaseInputCustomEvent<{ value: string }>) => {
    const { detail: { value } } = event;

    this.original = value;
  }

  handleChangeTranscription = (event: BaseInputCustomEvent<{ value: string }>) => {
    const { detail: { value } } = event;

    this.transcription = value;
  }

  handleChangeTranslation = (event: BaseInputCustomEvent<{ value: string }>) => {
    const { detail: { value } } = event;

    this.translation = value;
  }

  componentWillRender() {
    if (this.isEdit && this.editData) {
      const { original, transcription, translation } = this.editData;

      this.original = original;

      if (transcription) {
        this.transcription = transcription;
      }

      this.translation = translation;
    }
  }

  render() {
    if (this.isOpen) {
      console.log('this.isEdit', this.isEdit);

      return (
        <Tunnel.Consumer>
          {({ language }: TunnelState) => (
            <base-dialog
              onCloseDialog={this.handleClose}
              isOpen={this.isOpen}
              actions={[
                {
                  onClick: this.isEdit ? this.editWord : this.createWord,
                  text: translate(this.isEdit ? 'editWord' : 'createWord', language),
                },
              ]}
            >
              <h3 class='add-word-dialog-header-text' slot='headerText'>
                {translate(this.isEdit ? 'editWordDialogHeading' : 'addWordDialogHeading', language)}
              </h3>

              <div class='add-word-dialog-body'>
                <base-input
                  value={this.original}
                  inputId='original'
                  inputLabel={translate('originalLabel', language)}
                  additionalStyle={{ marginBottom: '8px' }}
                  onInputChange={this.handleChangeOriginal}
                />

                <base-input
                  value={this.transcription}
                  inputId='transcription'
                  inputLabel={translate('transcriptionLabel', language)}
                  additionalStyle={{ marginBottom: '8px' }}
                  onInputChange={this.handleChangeTranscription}
                />

                <base-input
                  value={this.translation}
                  inputId='translation'
                  inputLabel={translate('translationLabel', language)}
                  onInputChange={this.handleChangeTranslation}
                />
              </div>
            </base-dialog>
          )}
        </Tunnel.Consumer>
      );
    }

    return null;
  }
}

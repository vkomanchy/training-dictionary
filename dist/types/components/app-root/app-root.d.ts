import { Word } from '../../localStorage.utils';
export declare class AppRoot {
  language: string;
  theme: string;
  words: any[];
  mode: string;
  isAddWordDialogOpen: boolean;
  isRepeatSettingsDialogOpen: boolean;
  repeatWordsCount: number;
  changeTheme: () => void;
  setTheme: (theme: 'light' | 'dark') => void;
  changeLanguage: () => void;
  changeMode: (newMode: string) => void;
  _changeWords: (words: Word[]) => void;
  _handleOpenAddWordDialog: () => void;
  _handleCloseAddWordDialog: () => void;
  _handleOpenRepeatSettingsDialog: () => void;
  _handleCloseRepeatSettingsDialog: () => void;
  fetchWords: () => void;
  sortWordsByStats: () => void;
  changeRepeatWordsCount: (wordsCount: number) => void;
  componentWillLoad(): void;
  render(): any;
}

import { EventEmitter } from '../../stencil-public-runtime';
export interface DialogAction {
  onClick: () => void;
  text: string;
  isDisabled?: boolean;
}
export declare class BaseDialog {
  isOpen: boolean;
  actions: DialogAction[];
  closeDialog: EventEmitter<{}>;
  handleClickOutside: (e: any) => void;
  handleClose: () => void;
  render(): any;
}

import type { Components, JSX } from "../types/components";

interface BaseButton extends Components.BaseButton, HTMLElement {}
export const BaseButton: {
  prototype: BaseButton;
  new (): BaseButton;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;

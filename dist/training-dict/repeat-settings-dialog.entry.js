import { r as registerInstance, f as createEvent, h } from './index-a0476a79.js';
import { t as translate } from './translations.utils-32242b75.js';
import { T as Tunnel } from './state-e1173bff.js';

const repeatSettingsDialogCss = ":host{display:block}.repeat-settings-dialog-body{display:flex;flex-direction:column;padding:20px}.repeat-type-choose{display:flex;flex-direction:column;margin-bottom:12px}.repeat-type-option{display:flex;align-items:center;margin-bottom:8px}.repeat-stat p{margin:0;margin-top:8px}label{font-family:'Open Sans', sans-serif}.stat-label,.stat-value{font-family:'Open Sans', sans-serif}.repeat-settings-header-text{font-family:'Finlandica', sans-serif}";

const RepeatSettingsDialog = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.closeDialog = createEvent(this, "closeDialog", 7);
    this.repeatWordsCount = 1;
    this.selectedType = 'originalTranslate';
    this.handleClose = () => {
      this.closeDialog.emit();
    };
  }
  render() {
    if (this.isOpen) {
      return (h(Tunnel.Consumer, null, ({ language, words, changeRepeatWordsCount, changeMode, sortWordsByStats }) => {
        const repeatTypeOptions = [
          {
            label: translate('originalTranslate', language),
            value: 'originalTranslate',
          },
          {
            label: translate('reverseTranslate', language),
            value: 'reverseTranslate',
          }
        ];
        const handleChangeWordsCount = (event) => {
          const { detail: { value } } = event;
          this.repeatWordsCount = Number(value);
        };
        const handleChangeRepeatType = (value) => () => {
          this.selectedType = value;
        };
        const handleConfirmRepeat = () => {
          if (this.selectedType === 'originalTranslate') {
            changeMode('repeatWords');
          }
          else {
            changeMode('reverseRepeatWords');
          }
          sortWordsByStats();
          changeRepeatWordsCount(this.repeatWordsCount);
          this.handleClose();
        };
        return (h("base-dialog", { onCloseDialog: this.handleClose, isOpen: this.isOpen, actions: [
            {
              onClick: handleConfirmRepeat,
              text: translate('confirmRepeat', language),
              isDisabled: this.repeatWordsCount > words.length || this.repeatWordsCount <= 0 || isNaN(this.repeatWordsCount),
            },
          ] }, h("h3", { class: 'repeat-settings-header-text', slot: 'headerText' }, translate('repeatSettingsDialogHeading', language)), h("div", { class: 'repeat-settings-dialog-body' }, h("div", { class: 'repeat-type-choose' }, repeatTypeOptions.map(({ label, value }) => (h("div", { class: 'repeat-type-option' }, h("label", { htmlFor: value }, label), h("input", { type: 'radio', id: value, value: String(this.selectedType === value), checked: this.selectedType === value, onChange: handleChangeRepeatType(value) }))))), h("base-input", { value: String(this.repeatWordsCount), inputId: 'repeatWordsCount', inputLabel: translate('repeatWordsCountLabel', language), onInputChange: handleChangeWordsCount }), h("div", { class: 'repeat-stats-container' }, h("div", { class: 'repeat-stat' }, h("p", { class: 'stat-label' }, translate('totalWords', language), " - ", words.length))))));
      }));
    }
    return null;
  }
};
RepeatSettingsDialog.style = repeatSettingsDialogCss;

export { RepeatSettingsDialog as repeat_settings_dialog };

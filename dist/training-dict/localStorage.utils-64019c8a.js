const WORDS_KEY = 'words';
const MISTAKES_KEY = 'mistakes';
const getWordsFromLocalStorage = () => {
  const words = localStorage.getItem(WORDS_KEY);
  if (words) {
    return JSON.parse(words);
  }
  return;
};
const writeWordToLocalStorage = (word) => {
  if (window.localStorage) {
    const { localStorage } = window;
    const words = localStorage.getItem(WORDS_KEY);
    if (words) {
      const wordsArray = JSON.parse(words);
      localStorage.setItem(WORDS_KEY, JSON.stringify([...wordsArray, word]));
    }
    else {
      localStorage.setItem(WORDS_KEY, JSON.stringify([word]));
    }
  }
  else {
    throw new Error('localStorage is not defined. Sorry, you can use this app!');
  }
};
const deleteWordFromStorage = (id) => {
  if (window.localStorage) {
    const { localStorage } = window;
    const words = localStorage.getItem(WORDS_KEY);
    if (words) {
      const wordsArray = JSON.parse(words);
      const newWordsArray = wordsArray.filter(({ id: wordId }) => wordId !== id);
      if (wordsArray.length === newWordsArray.length) {
        console.error('Word was not found');
      }
      else {
        localStorage.setItem(WORDS_KEY, JSON.stringify(newWordsArray));
      }
    }
    else {
      console.error('Words list is empty');
    }
  }
  else {
    throw new Error('localStorage is not defined. Sorry, you can use this app!');
  }
};
const editWordInLocalStorage = (word) => {
  if (window.localStorage) {
    const { localStorage } = window;
    const { id } = word;
    const words = localStorage.getItem(WORDS_KEY);
    if (words) {
      const wordsArray = JSON.parse(words);
      const foundWordIndex = wordsArray.findIndex(({ id: wordId }) => wordId === id);
      if (foundWordIndex !== -1) {
        wordsArray[foundWordIndex] = word;
        localStorage.setItem(WORDS_KEY, JSON.stringify(wordsArray));
      }
      else {
        console.error('Error was not found');
      }
    }
    else {
      console.error('Words list is empty');
    }
  }
  else {
    throw new Error('localStorage is not defined. Sorry, you can use this app!');
  }
};
const dropStorage = () => {
  localStorage.clear();
};
const incrementWordMistake = (id) => {
  const mistakes = localStorage.getItem(MISTAKES_KEY);
  if (mistakes) {
    const mistakesObject = JSON.parse(mistakes);
    if (mistakesObject[id]) {
      mistakesObject[id] += 1;
    }
    else {
      mistakesObject[id] = 1;
    }
    localStorage.setItem(MISTAKES_KEY, JSON.stringify(mistakesObject));
  }
  else {
    const newMistakes = {
      [id]: 1,
    };
    localStorage.setItem(MISTAKES_KEY, JSON.stringify(newMistakes));
  }
};
const getMistakesStat = () => {
  const mistakes = localStorage.getItem(MISTAKES_KEY);
  if (mistakes) {
    return JSON.parse(mistakes);
  }
  else {
    console.log('Mistakes is empty!');
    return {};
  }
};

export { getMistakesStat as a, deleteWordFromStorage as d, editWordInLocalStorage as e, getWordsFromLocalStorage as g, incrementWordMistake as i, writeWordToLocalStorage as w };

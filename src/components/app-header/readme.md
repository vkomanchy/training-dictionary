# app-header



<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [app-root](../app-root)

### Depends on

- context-consumer

### Graph
```mermaid
graph TD;
  app-header --> context-consumer
  app-root --> app-header
  style app-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

import { Word } from '../../localStorage.utils';
export interface State {
  theme: string;
  language: string;
  changeLanguage: () => void;
  changeTheme: () => void;
  _changeWords: (words: Word[]) => void;
  changeMode: (newMode: string) => void;
  changeRepeatWordsCount: (wordsCount: number) => void;
  setTheme: (theme: 'light' | 'dark') => void;
  sortWordsByStats: () => void;
  mode: string;
  words: Word[];
  repeatWordsCount: number;
}
declare const _default: {
  Provider: import("@stencil/state-tunnel/dist/types/stencil.core").FunctionalComponent<{
    state: State;
  }>;
  Consumer: import("@stencil/state-tunnel/dist/types/stencil.core").FunctionalComponent<{}>;
  injectProps: (Cstr: any, fieldList: import("@stencil/state-tunnel/dist/types/declarations").PropList<State>) => void;
};
export default _default;

export interface Word {
  original: string;
  transcription?: string;
  translation: string;
  id: string;
}
export declare const getWordsFromLocalStorage: () => Word[] | undefined;
export declare const writeWordToLocalStorage: (word: Word) => void;
export declare const deleteWordFromStorage: (id: string) => void;
export declare const editWordInLocalStorage: (word: Word) => void;
export declare const dropStorage: () => void;
export declare const incrementWordMistake: (id: string) => void;
export declare const getMistakesStat: () => {
  [key: string]: number;
};

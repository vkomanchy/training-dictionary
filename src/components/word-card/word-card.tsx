import { Component, Event, EventEmitter, Fragment, h, Prop, State } from '@stencil/core';
import { translate } from '../../translations.utils';
import Tunnel, { State as TunnelState } from '../data/state';

import Edit from '../assets/edit.svg';
import Trash from '../assets/trash.svg';
import { incrementWordMistake } from '../../localStorage.utils';
import { BaseInputCustomEvent } from '../../components';

@Component({
  tag: 'word-card',
  styleUrl: 'word-card.css',
  shadow: true,
})
export class WordCard {
  @Prop() wordId: string;
  @Prop() original: string;
  @Prop() transcription?: string;
  @Prop() translation: string;

  @State() translateGuess: string = '';
  @State() guessType: 'repeatWords' | 'reverseRepeatWords' = 'repeatWords';

  @State() isOpen: boolean;

  @Event({
    eventName: 'deleteCard',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) deleteCard: EventEmitter<{ wordId: string; }>;

  @Event({
    eventName: 'editCard',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) editCard: EventEmitter<{}>;

  handleCloseEdit = () => {
    this.isOpen = false;

    this.editCard.emit();
  };

  handleClickEdit = () => {
    this.isOpen = true;
  }

  handleClickDelete = () => {
    this.deleteCard.emit({ wordId: this.wordId });
  }

  handleChangeTranslationGuess = (event: BaseInputCustomEvent<{ value: string }>) => {
    const { detail: { value } } = event;

    this.translateGuess = value;
  }

  render() {
    return (
      <Tunnel.Consumer>
        {({ mode, theme, language }: TunnelState) => {
          if (mode === 'repeatWords' || mode === 'reverseRepeatWords') {
            this.guessType = mode;
          }

          if (mode === 'view') {
            this.translateGuess = '';

            return (
              <div class={`word-card-container view-mode ${theme}-card-container`}>
                <div class={`word-card-header ${theme}-card-header`}>
                  <button class='word-card-button' onClick={this.handleClickEdit}>
                    <div class={`word-icon-container ${theme}-word-icon-container`} innerHTML={Edit} />
                  </button>

                  <button class='word-card-button' onClick={this.handleClickDelete}>
                    <div class={`word-icon-container ${theme}-word-icon-container`} innerHTML={Trash} />
                  </button>
                </div>

                <div class={`words-data ${theme}-words-data`}>
                  <p class={`word-label ${theme}-word-label`}>{translate('originalLabel', language)}</p>
                  <p class={`word-text ${theme}-word-text`}>{this.original}</p>

                  {this.transcription && (
                    <Fragment>
                      <p class={`word-label ${theme}-word-label`}>{translate('transcriptionLabel', language)}</p>
                      <p class={`word-text ${theme}-word-text`}>{this.transcription}</p>
                    </Fragment>
                  )}

                  <p class={`word-label ${theme}-word-label`}>{translate('translationLabel', language)}</p>
                  <p class={`word-text ${theme}-word-text`}>{this.translation}</p>
                </div>

                <add-word-dialog
                  isOpen={this.isOpen}
                  isEdit={true}
                  editData={{
                    original: this.original,
                    transcription: this.transcription,
                    translation: this.translation,
                    id: this.wordId,
                  }}
                  onCloseDialog={this.handleCloseEdit}
                />
              </div>
            );
          }

          if (mode === 'repeatWords' || mode === 'reverseRepeatWords') {
            return (
              <div class={`word-card-container view-mode ${theme}-card-container`}>
                <div class={`words-data ${theme}-words-data`}>
                  {
                    mode === 'reverseRepeatWords'
                      ? (
                        <Fragment>
                          <base-input
                            value={this.translateGuess}
                            onInputChange={this.handleChangeTranslationGuess}
                            inputId='translateGuess'
                            inputPlaceholder={translate('translateGuessLabel', language)}
                          />

                          <div class='empty-block'></div>
                        </Fragment>
                      )
                      : (
                        <Fragment>
                          <p class={`word-label ${theme}-word-label`}>{translate('originalLabel', language)}</p>
                          <p class={`word-text ${theme}-word-text`}>{this.original}</p>
                        </Fragment>
                      )
                  }

                  {this.transcription && (
                    <Fragment>
                      <p class={`word-label ${theme}-word-label`}>{translate('transcriptionLabel', language)}</p>
                      <p class={`word-text ${theme}-word-text`}>{this.transcription}</p>
                    </Fragment>
                  )}

                  {
                    mode === 'repeatWords'
                      ? (
                        <base-input
                          value={this.translateGuess}
                          onInputChange={this.handleChangeTranslationGuess}
                          inputId='translateGuess'
                          inputPlaceholder={translate('translateGuessLabel', language)}
                        />
                      )
                      : (
                        <Fragment>
                          <p class={`word-label ${theme}-word-label`}>{translate('translationLabel', language)}</p>
                          <p class={`word-text ${theme}-word-text`}>{this.translation}</p>
                        </Fragment>
                      )
                  }
                </div>
              </div>
            );
          }

          if (mode === 'checkWords') {
            const isCheckBasicTranslation = this.guessType === 'repeatWords';

            const lowerCasedTranslation = this.translation.toLowerCase();
            const lowerCasedOriginal = this.original.toLowerCase();

            const lowerCasedTranslateGuess = this.translateGuess.toLowerCase().trim();

            const isWordCorrect = isCheckBasicTranslation
              ? lowerCasedTranslation === lowerCasedTranslateGuess
              : lowerCasedOriginal === lowerCasedTranslateGuess;

            if (!isWordCorrect) {
              incrementWordMistake(this.wordId);
            }

            const originalClassName = (() => {
              if (!isCheckBasicTranslation) {
                return `word-text ${theme}-word-text ${isWordCorrect ? 'is-right' : 'is-wrong'}`;
              }

              return `word-text ${theme}-word-text`;
            })();

            const translationClassName = (() => {
              if (isCheckBasicTranslation) {
                return `word-text ${theme}-word-text ${isWordCorrect ? 'is-right' : 'is-wrong'}`;
              }

              return `word-text ${theme}-word-text`;
            })();

            return (
              <div class={`word-card-container view-mode ${theme}-card-container`}>
                <div class={`words-data ${theme}-words-data`}>
                  <p class={`word-label ${theme}-word-label`}>{translate('originalLabel', language)}</p>
                  <p class={originalClassName}>{!isCheckBasicTranslation ? this.translateGuess : this.original}</p>
                  {!isWordCorrect && !isCheckBasicTranslation && (
                    <Fragment>
                      <div class='empty-block'></div>
                      <p class='word-text is-right'>{this.original}</p>
                    </Fragment>
                  )}

                  {this.transcription && (
                    <Fragment>
                      <p class={`word-label ${theme}-word-label`}>{translate('transcriptionLabel', language)}</p>
                      <p class={`word-text ${theme}-word-text`}>{this.transcription}</p>
                    </Fragment>
                  )}

                  <p class={`word-label ${theme}-word-label`}>{translate('translationLabel', language)}</p>
                  <p class={translationClassName}>{isCheckBasicTranslation ? this.translateGuess : this.translation}</p>
                  {!isWordCorrect && isCheckBasicTranslation && (
                    <Fragment>
                      <div class='empty-block'></div>
                      <p class='word-text is-right'>{this.translation}</p>
                    </Fragment>
                  )}
                </div>
              </div>
            );
          }
        }}
      </Tunnel.Consumer>
    );
  }
}

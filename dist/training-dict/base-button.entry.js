import { r as registerInstance, f as createEvent, h, e as Host } from './index-a0476a79.js';
import { T as Tunnel } from './state-e1173bff.js';

const baseButtonCss = ":host{display:block}.button-container{border-radius:5px;border:none;height:35px;padding:0 10px;cursor:pointer;outline:none;transition-property:background;transition-duration:500ms}.dark-button-container{background:rgb(51, 53, 55);color:white}.dark-button-container:hover{background:rgb(72, 75, 79)}.light-button-container{background:#e9e9ed}.light-button-container:hover{background:#c7c7c8}.button-disabled{cursor:not-allowed}button{font-family:'Open Sans', sans-serif}";

const BaseButton = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.buttonClick = createEvent(this, "buttonClick", 7);
    this.additionalStyle = {};
    this.isDisabled = false;
    this.handleClick = (e) => {
      this.buttonClick.emit(e);
    };
  }
  render() {
    return (h(Tunnel.Consumer, null, ({ theme }) => (h(Host, null, h("button", { onClick: this.handleClick, disabled: this.isDisabled, class: `button-container ${theme}-button-container button-${this.isDisabled ? 'disabled' : 'enabled'}`, style: this.additionalStyle }, h("slot", null))))));
  }
};
BaseButton.style = baseButtonCss;

export { BaseButton as base_button };

import { r as registerInstance, h, e as Host } from './index-a0476a79.js';
import { g as getWordsFromLocalStorage, a as getMistakesStat } from './localStorage.utils-64019c8a.js';
import { i as initBrowserLang } from './translations.utils-32242b75.js';
import { T as Tunnel } from './state-e1173bff.js';

const appRootCss = ":host{display:block;height:100%}.buttons-block{display:flex}";

const AppRoot = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.language = initBrowserLang();
    this.theme = 'light';
    this.words = [];
    this.mode = 'view';
    this.isAddWordDialogOpen = false;
    this.isRepeatSettingsDialogOpen = false;
    this.repeatWordsCount = 0;
    this.changeTheme = () => {
      if (this.theme === 'light') {
        this.theme = 'dark';
      }
      else {
        this.theme = 'light';
      }
    };
    this.setTheme = (theme) => {
      this.theme = theme;
    };
    this.changeLanguage = () => {
      if (this.language === 'ru') {
        this.language = 'en';
      }
      else {
        this.language = 'ru';
      }
    };
    this.changeMode = (newMode) => {
      this.mode = newMode;
    };
    this._changeWords = (words) => {
      this.words = words;
    };
    this._handleOpenAddWordDialog = () => {
      this.isAddWordDialogOpen = true;
    };
    this._handleCloseAddWordDialog = () => {
      this.isAddWordDialogOpen = false;
      this.fetchWords();
    };
    this._handleOpenRepeatSettingsDialog = () => {
      this.isRepeatSettingsDialogOpen = true;
    };
    this._handleCloseRepeatSettingsDialog = () => {
      this.isRepeatSettingsDialogOpen = false;
    };
    this.fetchWords = () => {
      const words = getWordsFromLocalStorage();
      this._changeWords(words);
    };
    this.sortWordsByStats = () => {
      const mistakes = getMistakesStat();
      this.words.sort((a, b) => {
        if (mistakes[a.id] && mistakes[b.id]) {
          const aMistakes = mistakes[a.id];
          const bMistakes = mistakes[b.id];
          return bMistakes - aMistakes;
        }
        if (mistakes[a.id] && !mistakes[b.id]) {
          return -1;
        }
        if (!mistakes[a.id] && mistakes[b.id]) {
          return 1;
        }
        return 0;
      });
    };
    this.changeRepeatWordsCount = (wordsCount) => {
      this.repeatWordsCount = wordsCount;
    };
  }
  componentWillLoad() {
    this.fetchWords();
  }
  render() {
    const state = {
      theme: this.theme,
      language: this.language,
      words: this.words,
      mode: this.mode,
      repeatWordsCount: this.repeatWordsCount,
      changeTheme: this.changeTheme,
      setTheme: this.setTheme,
      changeLanguage: this.changeLanguage,
      sortWordsByStats: this.sortWordsByStats,
      changeMode: this.changeMode,
      changeRepeatWordsCount: this.changeRepeatWordsCount,
      _changeWords: this._changeWords,
      fetchWords: this.fetchWords,
    };
    return (h(Host, { class: `root-${this.theme}` }, h(Tunnel.Provider, { state: state }, h("app-header", null, "Training Dictionary"), h("app-controls", { onOpenAddWordDialog: this._handleOpenAddWordDialog, onOpenRepeatSettingsDialog: this._handleOpenRepeatSettingsDialog }), h("dictionary-words", { onUpdateCards: this.fetchWords }), h("add-word-dialog", { isOpen: this.isAddWordDialogOpen, onCloseDialog: this._handleCloseAddWordDialog }), h("repeat-settings-dialog", { isOpen: this.isRepeatSettingsDialogOpen, onCloseDialog: this._handleCloseRepeatSettingsDialog }))));
  }
};
AppRoot.style = appRootCss;

export { AppRoot as app_root };

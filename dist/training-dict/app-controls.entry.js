import { r as registerInstance, f as createEvent, h, F as Fragment } from './index-a0476a79.js';
import { T as Tunnel } from './state-e1173bff.js';
import { t as translate } from './translations.utils-32242b75.js';

const appControlsCss = ".app-controls-container{display:flex;height:50px;align-items:center}";

const AppControls = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.openAddWordDialog = createEvent(this, "openAddWordDialog", 7);
    this.openRepeatSettingsDialog = createEvent(this, "openRepeatSettingsDialog", 7);
    this.handleClickAddWord = () => {
      this.openAddWordDialog.emit();
    };
    this.handleClickRepeatWords = () => {
      this.openRepeatSettingsDialog.emit();
    };
  }
  render() {
    return (h(Tunnel.Consumer, null, ({ language, mode, changeMode }) => {
      const handleCheckWords = () => {
        changeMode('checkWords');
      };
      const handleStopRepeating = () => {
        changeMode('view');
      };
      return (h("div", { class: "app-controls-container" }, mode === 'view' && (h(Fragment, null, h("base-button", { onClick: this.handleClickAddWord, additionalStyle: { margin: '0 8px 0 18px' } }, translate('addWord', language)), h("base-button", { onClick: this.handleClickRepeatWords }, translate('repeatWords', language)))), (mode === 'repeatWords' || mode === 'reverseRepeatWords') && (h(Fragment, null, h("base-button", { onClick: handleCheckWords, additionalStyle: { margin: '0 8px 0 18px' } }, translate('checkWords', language)), h("base-button", { onClick: handleStopRepeating }, translate('endRepeat', language)))), mode === 'checkWords' && (h("base-button", { onClick: handleStopRepeating, additionalStyle: { margin: '0 8px 0 18px' } }, translate('endRepeat', language)))));
    }));
  }
};
AppControls.style = appControlsCss;

export { AppControls as app_controls };

import { Component, Host, h, State } from '@stencil/core';
import Tunnel, { State as TunnelState } from '../data/state';
import Sun from '../assets/sun.svg';
import Moon from '../assets/moon.svg';
import { translate } from '../../translations.utils';
import { getUserTheme } from '../../getUserTheme';

@Component({
  tag: 'app-header',
  styleUrl: 'app-header.css',
  shadow: true,
})
export class AppHeader {
  @State() isInitialThemeSettled = false;

  render() {
    return (
      <Tunnel.Consumer>
        {({ theme, language, setTheme, changeTheme, changeLanguage }: TunnelState) => {
          if (!this.isInitialThemeSettled) {
            setTheme(getUserTheme());
          }

          this.isInitialThemeSettled = true;

          return (
            <Host>
              <header class={`app-header app-header-${theme}`}>
                <h1 class={`app-heading app-heading-${theme}`}>
                  <slot></slot>
                </h1>

                <div class='buttons-block'>
                  <div class="theme-switcher-container" title={translate(theme === 'light' ? 'switchToDarkTheme' : 'switchToLightTheme', language)}>
                    <button onClick={changeTheme} class="theme-switcher">
                      {theme === 'light'
                        ? <div class='icon-container light-icon-container' innerHTML={Moon} />
                        : <div class='icon-container dark-icon-container' innerHTML={Sun} />}
                    </button>
                  </div>

                  <div class="language-switcher-container" title={translate('switchLanguage', language)}>
                    <button onClick={changeLanguage} class="language-switcher">
                      {language === 'ru'
                        ? <div class={`word-container ${theme}-word-container`}>RU</div>
                        : <div class={`word-container ${theme}-word-container`}>EN</div>}
                    </button>
                  </div>
                </div>
              </header>
            </Host>
          );
        }}
      </Tunnel.Consumer>
    );
  }
}

Tunnel.injectProps(AppHeader, ['theme', 'language']);

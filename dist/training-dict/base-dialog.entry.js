import { r as registerInstance, f as createEvent, h, e as Host } from './index-a0476a79.js';
import { T as Tunnel } from './state-e1173bff.js';
import { t as translate } from './translations.utils-32242b75.js';

const baseDialogCss = ":host{height:100%;width:100%}.dialog{position:absolute;height:100%;width:100%;top:0;left:0;background:rgba(69, 75, 85, 0.54)}.dialog-inner{min-height:380px;min-width:340px;background:white;position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);display:flex;flex-direction:column;border-radius:5px}.dialog-header{flex-grow:0;flex-shrink:0;flex-basis:10%;padding:0 10px}.dialog-body{flex-grow:1;flex-shrink:0;flex-basis:80%}.dialog-footer{flex-grow:0;flex-shrink:0;flex-basis:10%;display:flex;justify-content:flex-end;padding:10px}";

;
const BaseDialog = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.closeDialog = createEvent(this, "closeDialog", 7);
    this.handleClickOutside = (e) => {
      if (e.target.className === 'dialog') {
        this.closeDialog.emit();
      }
    };
    this.handleClose = () => {
      this.closeDialog.emit();
    };
  }
  render() {
    return (h(Tunnel.Consumer, null, ({ language }) => (h(Host, null, h("div", { class: "dialog", onClick: this.handleClickOutside }, h("div", { class: "dialog-inner" }, h("div", { class: "dialog-header" }, h("slot", { name: 'headerText' })), h("div", { class: 'dialog-body' }, h("slot", null)), h("div", { class: 'dialog-footer' }, [
      ...this.actions,
      {
        onClick: this.handleClose,
        text: translate('closeDialog', language)
      },
    ]
      .map(({ onClick, text, isDisabled }) => (h("base-button", { onButtonClick: onClick, additionalStyle: { marginLeft: '8px' }, isDisabled: isDisabled }, text))))))))));
  }
};
BaseDialog.style = baseDialogCss;

export { BaseDialog as base_dialog };

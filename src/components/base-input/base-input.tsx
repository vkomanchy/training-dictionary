import { Component, Host, h, Prop, EventEmitter, Event } from '@stencil/core';

@Component({
  tag: 'base-input',
  styleUrl: 'base-input.css',
  shadow: true,
})
export class BaseInput {
  @Event({
    eventName: 'inputChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) inputChange: EventEmitter<{ value: string; }>;

  @Prop() value: string;
  @Prop() inputId: string;
  @Prop() inputLabel: string;
  @Prop() inputPlaceholder: string;

  @Prop() additionalStyle: { [key: string]: string; } = {};

  handleChange = (e: KeyboardEvent) => {
    this.inputChange.emit({ value: (e.target as HTMLInputElement).value });
  };

  render() {
    return (
      <Host style={this.additionalStyle}>
        {this.inputLabel && <label htmlFor={this.inputId}>{this.inputLabel}</label>}

        <input
          type='text'
          id={this.inputId}
          placeholder={this.inputPlaceholder}
          value={this.value}
          onInput={this.handleChange}
        />
      </Host>
    );
  }

}

import type { Components, JSX } from "../types/components";

interface AppControls extends Components.AppControls, HTMLElement {}
export const AppControls: {
  prototype: AppControls;
  new (): AppControls;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
